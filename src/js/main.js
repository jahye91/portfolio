var scene = document.getElementById('scene');


//로딩후
$(window).load(function(){
  fixedMenu();
  autoType(".type-js",200);

});

setTimeout( function () {
  $("#loading").fadeOut();
},8500);



function autoType(elementClass, typingSpeed){
  var thhis = $(elementClass);
  thhis.css({
    "position": "relative",
    "display": "inline-block"
  });
  thhis.prepend('<div class="cursor" style="right: initial; left:0;"></div>');
  thhis = thhis.find(".text-js");
  var text = thhis.text().trim().split('');
  var amntOfChars = text.length;
  var newString = "";
  thhis.text("|");
  setTimeout(function(){
    thhis.css("opacity",1);
    thhis.prev().removeAttr("style");
    thhis.text("");
    for(var i = 0; i < amntOfChars; i++){
      (function(i,char){
        setTimeout(function() {
          newString += char;
          thhis.text(newString);
        },i*typingSpeed);
      })(i+1,text[i]);
    }
  },1500);
}

function scrollEvent() {
  window.onload = function () {
    $(".section").each(function () {
      // 개별적으로 Wheel 이벤트 적용
      $(this).on("mousewheel DOMMouseScroll", function (e) {
        e.preventDefault();
        var delta = 0;
        if (!event) event = window.event;
        if (event.wheelDelta) {
          delta = event.wheelDelta / 120;
          if (window.opera) delta = -delta;
        } else if (event.detail) delta = -event.detail / 3;
        var moveTop = null;
        // 마우스휠을 위에서 아래로
        if (delta < 0) {
          if ($(this).next() != undefined) {
            moveTop = $(this).next().offset().top;
          }
          // 마우스휠을 아래에서 위로
        } else {
          if ($(this).prev() != undefined) {
            moveTop = $(this).prev().offset().top;
          }
        }
        // 화면 이동 0.8초(800)
        $("html,body").stop().animate({
          scrollTop: moveTop + 'px'
        }, {
          duration: 800, complete: function () {
          }
        });
      });
    });
  }
}

function sideNav() {
  var $nav = $("#header__menu");
  var $tar = $("[id*='step']");

  if ($nav.hasClass("active")) {
    $(window).scroll(function () {
      var windowTop = $(window).scrollTop();
      $tar.each(function (idx) {

        if (windowTop >= $(this).offset().top - 10) {
          $nav.find("a").eq(idx).addClass("active").siblings().removeClass("active");
          var $this = $(this);
          $this.addClass("active").siblings().removeClass("active");

        }
      })
    });
  }

  $nav.on("click", "a", function () {
    var $tar = $(this).attr("href");
    $("html,body").stop().animate({
      scrollTop: ($($tar).offset().top)
    }, 800);
    return false;
  });
}




$(window).scroll( function () {
  var $sec = $(".section.active");
  var $secTxt = $sec.attr("id");

  $("header").removeClass("step01");
  $("header").removeClass("step02");
  $("header").removeClass("step03");
  $("header").removeClass("step04");
  $("header").removeClass("step05");

  if($sec.hasClass("active")) {
    $("header").addClass($secTxt);
  }

});

function allMenu() {
  $(".header__menuHide").show();

}
function allMenuClose() {
  $(".header__menuHide").hide();

}
$(document).ready( function () {
  $(".header__menuHideItem").mouseenter( function () {
    $except = $(".header__menuHideItem");
    $except.addClass("blur");
    $this = $(this);
    $this.removeClass("blur");
  });
  $(".header__menuHideList").mouseleave( function () {
    $except.removeClass("blur");
  });
});
function fixedMenu() {
  $(window).scroll( function () {
    var $sec = $(".section.active");
    var $secTxt = $sec.attr("id");

    $("header").removeClass("step01,step02,step03,step04,step05");

    if($sec.hasClass("active")) {
      $("header").addClass($secTxt);
    }

  });
}


